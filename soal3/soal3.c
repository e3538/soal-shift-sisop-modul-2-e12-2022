#include <dirent.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <wait.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <pwd.h>

void initFolderDarat()
{
    // membuat folder main-task di /home/raul/shift2/drakor
   	 int status = 0;
   	 if(fork()==0)
   	 {
   		 char *argv[] = {"mkdir", "-p", "/home/afdal/modul2/darat", NULL};
   		 execv("/bin/mkdir", argv);
   	 }
   	 while(wait(&status)>0);
}

void initFolderAir()
{
    // membuat folder main-task di /home/raul/shift2/drakor
   	 int status = 0;
   	 if(fork()==0)
   	 {
   		 char *argv[] = {"mkdir", "-p", "/home/afdal/modul2/air", NULL};
   		 execv("/bin/mkdir", argv);
   	 }
   	 while(wait(&status)>0);
}

void unzipFile(){
    int status = 0;
   	 if(fork()==0)
   	 {
   		char *argv[] = {"unzip", "-q", "-o", "/home/afdal/animal.zip", "-d", "/home/afdal/modul2", NULL};
        execv("/bin/unzip", argv);
   	 }
   	 while(wait(&status)>0); 
}

void moveFile(char path[], char file[]){
     int status = 0;
   	 if(fork()==0)
   	 {
   		char *argv[] = {"find", "/home/afdal/modul2/animal", "-name", file, "-exec", "mv", "-t",path, "{}","+",NULL };
        execv("/bin/find", argv);
   	 }
   	 while(wait(&status)>0); 
}

void deleteFile(){
    int status = 0;
   	 if(fork()==0)
   	 {
   		char *argv[] = {"find", "/home/afdal/modul2/animal", "-name", "*.jpg*", "-exec", "rm", "{}","+",NULL };
        execv("/bin/find", argv);
   	 }
   	 while(wait(&status)>0); 
}

void deleteBird(){
   int status = 0;
   	 if(fork()==0)
   	 {
   		char *argv[] = {"find", "/home/afdal/modul2/darat", "-name", "*bird*", "-exec", "rm", "{}","+",NULL };
        execv("/bin/find", argv);
   	 }
   	 while(wait(&status)>0);  
}

void listFile(){
    FILE *fptr;
    struct dirent *dt;
    char path[] = "/home/afdal/modul2/air/";
    DIR *dir = opendir(path);

    fptr = fopen("/home/afdal/modul2/air/list.txt", "w");
    if(fptr == NULL){
      printf("ERROR!\n");
      exit(1);
    }

    while ((dt = readdir(dir)) != NULL){
      struct stat fs;
      int r = stat(path, &fs);
      if(r == -1){
        fprintf(stderr, "File Error\n");
        exit(1);
      }
      register struct passwd *pw;
      uid_t uid = getuid();
      pw = getpwuid(uid);
      if(strcmp(dt->d_name,".") != 0 && strcmp(dt->d_name, "..") != 0 && strcmp(dt->d_name, "list.txt") != 0){
        char userp[256] = "";
        if(fs.st_mode & S_IRUSR)
          strcat(userp, "r");
        if(fs.st_mode & S_IWUSR)
          strcat(userp, "w");
        if(fs.st_mode & S_IXUSR)
          strcat(userp, "x");
        fprintf(fptr,"%s_%s_%s\n",pw->pw_name,userp, dt->d_name);
      }
    }
    fclose(fptr);
}

void master(char path[]){
    pid_t child_id;
    int status = 0;
    child_id = fork();
    if(child_id < 0)
        exit(EXIT_FAILURE);
    else if (child_id == 0){
        printf("Membuat folder darat...\n");
        initFolderDarat();
        
    }else {
        pid_t child_id2;
        int status2 = 0;
        child_id2 = fork();
        while(wait(&status)>0);
        sleep(3);
        if (child_id2 == 0){
            printf("Membuat folder air...\n");
            initFolderAir();
        }else{
            pid_t child_id3;
            int status3 = 0;
            child_id3 = fork();
            if(child_id3 == 0){
                printf("Unzipping animal.zip ...\n");
                unzipFile();
            }else{
               int status4 = 0;
               while(wait(&status3) > 0);
               if(fork() == 0){
                   char pathAir[] = "/home/afdal/modul2/air";
                   char air[] = "*air*";
                   char pathDarat[] = "/home/afdal/modul2/darat";
                   char darat[] = "*darat*";

                   printf("Moving air animal to air folder\n");
                   moveFile(pathAir, air);

                   printf("Moving darat animal to darat folder\n");
                   moveFile(pathDarat, darat);

                   printf("Deleting the rest\n");
                   deleteFile();
               }else{
                   int status5 = 0;
                   while(wait(&status4) > 0);
                   if(fork() == 0){
                       printf("Deleting birds\n");
                       deleteBird();
                   }else{
                       while(wait(&status5) > 0);
                       listFile();
                   }
               }
            }
        }
    }
}

int main(){
    char path[] = "/home/afdal/modul2";
    master(path);
}
