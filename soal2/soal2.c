#include <dirent.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <wait.h>

typedef struct film
{
    char judul[50];
    int tahun;
} film;

int catN = 0;
char category[20][20];

void initFolder()
{
    // membuat folder main-task di /home/raul/shift2/drakor
    int status = 0;
    if(fork()==0)
    {
        char buf1[256];
        snprintf(buf1, sizeof buf1, "shift2/drakor/");
        char *argv[] = {"mkdir", "-p", buf1, NULL};
        execv("/bin/mkdir", argv);
    }
    while(wait(&status)>0);
}

void categoryFolder(char *category)
{
    // membuat folder baru sesuai dengan kategori/genre-nya
    int status = 0;
    if(fork()==0)
    {
        char buf1[256];
        char *kategori = strtok(category, ".");
        snprintf(buf1, sizeof buf1, "shift2/drakor/%s", kategori);
        char *argv[] = {"mkdir", "-p", buf1, NULL};
        execv("/bin/mkdir", argv);
    }
    while(wait(&status)>0);
}

void moveFiles(char *filename, char *category, char *namafile)
{
    // memindahkan file hasil ekstrak ke dalam folder kategori sesuai genre
    int status = 0;
    char buf1[256], buf2[256];
    char *kategori = strtok(category, ".");
    snprintf(buf1, sizeof buf1, "shift2/drakor/%s", namafile);
    snprintf(buf2, sizeof buf2, "shift2/drakor/%s/%s.png", kategori, filename);
    if(fork()==0)
    {
        char *argv[] = {"cp", "-r", buf1, buf2, NULL};
        execv("/bin/cp", argv);
    }
    while(wait(&status)>0);
}

void sortByTahun(film curr[], int n)
{
    // sort data drakor berdasarkan tahun
    film drakortemp;
    for(int i=0;i<n;i++) for(int j=i+1;j<n;j++)
    if(curr[i].tahun > curr[j].tahun)
        drakortemp = curr[i], curr[i] = curr[j], curr[j] = drakortemp;
}

void printData(film curr[], int n)
{
    // ngeprint semua data yang ada pada struct untuk testing
    for(int i=0;i<n;i++) if(curr[i].tahun > 0) printf("Judul: %s\nTahun: %d\n\n", curr[i].judul, curr[i].tahun);
}

void deletePrevData(char *kategori)
{
    // hapus file data_prev di setiap kategori
    if(fork()==0)
    {
        char path[100] = "shift2/drakor/";
        strcat(path, kategori);
        chdir(path);
        char *argv[] = {"rm", "data_prev.txt", NULL};
        execv("/bin/rm", argv);
        printf("deleting..\n");
    }
}

void writeData(film sorted[], int n, char kategori[])
{
    // tulis balik filenya
    for(int i=0; i<n; i++)
    {
        char buf1[10000];
        snprintf(buf1, sizeof buf1, "shift2/drakor/%s/data.txt", kategori);
        char path[100] = "shift2/drakor/";
        strcat(path, kategori);
        char filename[10] = "/data.txt";
        strcat(path, filename);
        FILE *data;
        printf("%s\n", buf1);
        if(sorted[i].tahun > 0 && sorted[i].tahun <= 2022)
        {
            if(data = fopen(path, "r"))
            {
                data = fopen(buf1, "a+");
                fprintf(data, "\nnama : %s", sorted[i].judul);
                fprintf(data, "rilis : tahun %d", sorted[i].tahun);
                fprintf(data, "\n");
                fclose(data);
            }
            else
            {
                data = fopen(buf1, "a+");
                fprintf(data, "kategori : %s", kategori);
                fprintf(data, "\n\n");
                fprintf(data, "nama : %s", sorted[i].judul);
                fprintf(data, "rilis : tahun %d", sorted[i].tahun);
                fprintf(data, "\n");
                fclose(data);
            }
        }
    }
}

void readData()
{
    // baca data dari text file di masing-masing kategori, setor data ke struct, dan panggil sort untuk mengurutkan
    for(int i=0; i<catN; i++)
    {
        char path[100] = "shift2/drakor/";
        strcat(path, category[i]);
        char filename[20] = "/data_prev.txt";
        strcat(path, filename);
        FILE *fp = fopen(path, "r");
        const unsigned MAX_LENGTH = 256;
        char buffer[MAX_LENGTH];

        int counter = 1, drakorCounter = 0;
        film drakor[20];
        while(fgets(buffer, MAX_LENGTH, fp))
        {
            if(counter%3==0)
            {
                char judul[50];
                char *token = strtok(buffer, " ");
                while(token!=NULL) strcpy(judul, token), token = strtok(NULL, " ");
                if(strcmp(judul," ") != 0)
                    strcpy(drakor[drakorCounter].judul, judul);
            }
            else if(counter%3==1 && counter>3)
            {
                char read[50];
                char *token = strtok(buffer, " ");
                while(token!=NULL) strcpy(read, token), token = strtok(NULL, " ");
                if(atoi(read) > 0)
                    drakor[drakorCounter++].tahun = atoi(read);
            }
            counter++;
        }
        // panggil sort
        sortByTahun(drakor, drakorCounter);
        printf("KATEGORI %s:\n", category[i]);
        printData(drakor, drakorCounter);
        deletePrevData(category[i]);
        writeData(drakor, drakorCounter, category[i]);
        fclose(fp);
    }
}

void storeData(char *judulDrakor, char *tahun, char *kategori_input)
{
    // setor data ke file .txt (data_prev.txt -> file .txt sementara)
    char *kat = strtok(kategori_input, "."), kategori[100], buf1[10000];
    char path[100] = "shift2/drakor/", filename[20] = "/data_prev.txt";
    int status = 0;
    strcpy(kategori, kat);
    snprintf(buf1, sizeof buf1, "shift2/drakor/%s/data_prev.txt", kategori);
    strcat(path, kategori);
    strcat(path, filename);
    FILE *data;
    if (data = fopen(path, "r"))
    {
        data = fopen(buf1, "a+");
        fprintf(data, "nama : %s", judulDrakor);
        fprintf(data, "\n");
        fprintf(data, "rilis : tahun %s", tahun);
        fprintf(data, "\n\n");
        fclose(data);
    }
    else
    {
        strcpy(category[catN++], kategori);
        data = fopen(buf1, "a+");
        fprintf(data, "kategori : %s", kategori);
        fprintf(data, "\n\n");
        fprintf(data, "nama : %s", judulDrakor);
        fprintf(data, "\n");
        fprintf(data, "rilis : tahun %s", tahun);
        fprintf(data, "\n\n");
        fclose(data);
    }
}

void delFiles(char *namafile)
{
    if(fork()==0)
    {
        chdir("/home/raul/shift2/drakor");
        char *argv[] = {"rm", namafile, NULL};
        execv("/bin/rm", argv);
    }
}

void master(char *base)
{
    // file master pengelola file utama -> membuat folder, memindahkan file, dan menyetor data
    char path[1000];
    struct dirent *dt;
    DIR *dir = opendir(base);
    char *token;
    if (!dir) return;
    while ((dt = readdir(dir)) != NULL)
    {
        if (strcmp(dt->d_name, ".") != 0 && strcmp(dt->d_name, "..") != 0)
        {
            char namafile[700], *namefile;
            namefile = dt->d_name;
            snprintf(namafile, sizeof namafile, "%s", dt->d_name);
            token  = strtok(namafile, ";_");
            char *data[2][3];
            int n=0;
            while(token != NULL)
            {
                int i = 0;
                while(i<3)
                    data[n][i++] = token,
                    token = strtok(NULL, ";_");
                n++;
            }
            for(int i=0; i<n; i++)
                categoryFolder(data[i][2]),
                moveFiles(data[i][0], data[i][2], namefile),
                storeData(data[i][0], data[i][1], data[i][2]);
            delFiles(namefile);
            strcpy(path, base); strcat(path, "/"); strcat(path, dt->d_name);
            master(path);
        }
    }
    readData();
    closedir(dir);
}

int main()
{
    // main function
    char path[30] = "/home/raul/shift2/drakor";
    id_t child_id;
    int status=0;
    child_id = fork();
    if (child_id < 0) exit(EXIT_FAILURE);
    if (child_id == 0)
    {
        initFolder();
        char *argv[] = {"unzip", "drakor.zip", "*.png", "-d", path, NULL};
        execv("/bin/unzip", argv);
    }
    else
    {
        while(wait(&status) > 0);
        master(path);
    }
    return 0;
}
