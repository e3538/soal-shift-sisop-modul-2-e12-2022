|     NRP    |     Nama    |
| :--------- |:--------    |
| 5025201076 | Raul Ilma Rajasa |
| 5025201163 | Muhammad Afdal Abdallah |
| 5025201202 | Aiffah Kiysa Waafi |


# soal shift sisop modul 2 E12 2022

# Soal 1
Mas Refadi yang menyukai game bengshin impek ingin membuat sebuah program untuk mensimulasi sistem history gacha item pada game tersebut. Program yang dibuat harus sesuai dengan beberapa hal berikut

1. Saat program pertama kali berjalan. program akan mendownload file characters dan file weapons dari link yang disediakan, lalu program akan mengekstrak kedua file. Kemudian membuat folder bernama "gacha_gacha"
2. Setiap jumlah gachanya genap akan dilakukan gacha item weapons, jika ganjil maka dilakukan gacha item characters. Setiap jumlah gacha nya mod 10, maka dibuat file baru (.txt) dan output hasil gacha selanjutnya akan berada dalam file baru tersebut. Setiap kali jumlah gacha nya mod 90, maka dibuat sebuah folder baru dan file (.tx) selanjutnya akan berada dalam folder baru tersebut. Setiap folder akan ada 9 file (.txt) yang berisi 10 hasil gacha.
3. Format penamaan setiap file (.txt) nya adalah {Hh:Mm:Ss}_gacha_{jumlah-gacha}, dan format penamaan untuk setiap folder nya adalah total_gacha_{jumlah-gacha}. Dan untuk setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar 1 second.
4. Untuk melakukan gacha item, harus menggunakan alat tukar yang dinamakan primogems. Satu kali gacha item akan menghabiskan 160 primogems. Primogems di awal di-define sebanyak 79000 primogems. Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu name dan rarity. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}. Program akan selalu melakukan gacha hingga primogems habis.
5. Proses untuk melakukan gacha item akan dimulai pada 30 Maret jam 04:44. Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah semua isi di folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)

## Penjelasan code soal 1
Pada fungsi main, pertama kali akan dipanggil fungsi setupDB dan makeFolder
```c
int main()
{
    pid_t pidUtama = getpid();
    setupDB();
    if(pidUtama != getpid()) exit(EXIT_SUCCESS);
    makeFolder("gacha_gacha");
```
Didalam fungsi setupDB program akan mendownload file weapons dan character dari link.
```c
void setupDB()
{
    pid_t procTunggu, pid1 = fork(), pid2 = fork();
    char db_char[] = "https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download";
    char db_weapon[] = "https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download";
    char charzip[] = "char.zip", weaponzip[] = "weapon.zip";
    if(pid1 > 0 && pid2 > 0)
    {
        //printf("Wget: %d %d\n", pid1, pid2);
        procTunggu = wait(NULL);
        //printf("Menunggu: (%d)\n", procTunggu);
        procTunggu = wait(NULL);
        //printf("Menunggu: (%d)\n", procTunggu);
        unzipDB();
    }
    else if(pid1 == 0 && pid2 > 0)
    {
        char *args[] = {"/bin/wget", "wget", "--quiet" , "--no-check-certificate", db_char, "-O", charzip, NULL};
        if(execv(args[0],args+1) == -1) perror("Wget buat gacha zip characters gagal."), exit(EXIT_FAILURE);
    }
    else if(pid1 > 0 && pid2 == 0)
    {
        char *args[] = {"/bin/wget", "wget", "--quiet" , "--no-check-certificate", db_weapon, "-O", weaponzip, NULL};
        if(execv(args[0],args+1) == -1) perror("Wget buat gacha zip weapons gagal."), exit(EXIT_FAILURE);
    }
    else if(pid1 < 0 && pid2 < 0) perror("Child buat wget gagal."), exit(EXIT_FAILURE);
}
```
Jika download berhasil dilakukan maka program akan mengunzip file pada fungsi unzipDB() seperti berikut
```c
void unzipDB()
{
    pid_t procTunggu, pid1 = fork(), pid2 = fork();
    if(pid1 > 0 && pid2 > 0)
    {
        procTunggu = printf("Mengunzip: %d %d\n", pid1, pid2);
        wait(NULL);
        //printf("Menunggu: (%d)\n", procTunggu);
        procTunggu = wait(NULL);
        //printf("Menunggu: (%d)\n", procTunggu);
        removeZip();
    }
    else if(pid1 == 0 && pid2 > 0)
    {
        char *args[] = {"/bin/unzip", "unzip", "-qq", "-n", "char.zip", NULL};
        if(execv(args[0],args+1) == -1) perror("Gagal mengunzil char.zip"), exit(EXIT_FAILURE);
    }
    else if(pid1 > 0 && pid2 == 0)
    {
        char *args[] = {"/bin/unzip", "unzip", "-qq", "-n", "weapon.zip", NULL};
        if(execv(args[0],args+1) == -1) perror("Gagal mengunzip weapon.zip"), exit(EXIT_FAILURE);
    }
    else if(pid1 < 0 && pid2 < 0) perror("Child buat unzip error."), exit(EXIT_FAILURE);
}
```
Jika program selesai mengunzip file yang didownload, kemudian program akan menghapus file zip weapons dan character menggunakan fungsi removeZip()
```c
void removeZip()
{
    // buang zipnya
    pid_t procTunggu, pid1 = fork(), pid2 = fork();
    if(pid1 > 0 && pid2 > 0)
    {
        //printf("Removing: %d %d\n", pid1, pid2);
        procTunggu = wait(NULL);
        //printf("Menunggu: (%d)\n", procTunggu);
        procTunggu = wait(NULL);
        //printf("Menunggu: (%d)\n", procTunggu);
    }
    else if(pid1 == 0 && pid2 > 0)
    {
        char *args[] = {"/bin/rm", "rm", "-f", "char.zip", NULL};
        if(execv(args[0],args+1) == -1) perror("Gagal menghapus char.zip"), exit(EXIT_FAILURE);
    }
    else if(pid1 > 0 && pid2 == 0)
    {
        char *args[] = {"/bin/rm", "rm", "-f", "weapon.zip", NULL};
        if(execv(args[0],args+1) == -1) perror("Gagal menghapus weapon.zip"), exit(EXIT_FAILURE);
    }
    else if(pid1 < 0 && pid2 < 0) perror("Child removing (rm) error."), exit(EXIT_FAILURE);
}
```
Kemudian menggunakan fungsi makeFolder akan dibuat sebuah folder bernama gacha_gacha menggunakan fork exec.
```c
void makeFolder(char *path)
{
    // membuat folder custom
    int status = 0;
    if(fork()==0)
    {
        char buf1[256];
        snprintf(buf1, sizeof buf1, "%s", path);
        char *argv[] = {"mkdir", "-p", buf1, NULL};
        execv("/bin/mkdir", argv);
    }
    while(wait(&status)>0);
}
```
Kemudian jika timeinfo sama dengan 30 maret jam 4.44, dengan menggunakan loop while program akan berjalan jika primogems lebih besar dari 160, jika kurang dari 160 program gacha akan berhenti, dan setiap gacha berjalan primogems akan berkurang sebanyak 160. Lalu program mengecek apakah jumlah gacha nya mod 90, jika iya maka akan dibuat sebuah folder baru dengan nama total_gacha_{jumlah-gacha}.
```c
if(gacha%90==0)
{
    memset(currFolder, '\0', 200*(sizeof(currFolder[0])));
    strcpy(currFolder, "gacha_gacha/");
    char folderName[100] = "total_gacha_";
    char numGacha[100];
    sprintf(numGacha, "%d", gacha);
    strcat(folderName, numGacha);
    strcat(currFolder, folderName);
    makeFolder(currFolder);
    gacha++;
}
```
Jika hasil gachanya mod 10 maka akan dibuat sebuah file (.txt) dengan format {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}
```c
else if(gacha%10==0)
{
    int x = 0;
    for(int i = gacha; i < gacha + 90; i++)
    {
        if(primogems < 160) break;
        printf("primo: %d\n", primogems);
        time_t current_time;
        struct tm * time_info;
        char filename[100];
        time(&current_time);
        time_info = localtime(&current_time);
        strftime(filename, sizeof(filename), "%H:%M:%S", time_info);
        strcat(filename, "_gacha_");
        char numGacha[100];
        sprintf(numGacha, "%d", i-x-10);
        strcat(filename, numGacha); strcat(filename, ".txt");
```
Kemudian dilakukan looping sebanyak 10 kali, jika jumlah gacha genap maka dilakukan print gacha weapons, dan jika jumlah gachanya ganjil dilakukan print gacha character. hasilnya akan berada di file (.txt) yang dibuat sebelumnya. Karena file database berformat (.json) maka program harus membaca dan parsing file (.json) dengan cara sebagai berikut
```c
for(int j=0; j<10;j++)
{
    if (primogems >= 160 && !done)
    {
        primogems-=160;
        char buf1[10000], counter[100];
        sprintf(counter, "%d", i-x-9);
        char pathFolder[100] = "gacha_gacha", outputFile[200] = "";
        strcat(outputFile, counter);
        strcat(outputFile, "_");
        char tipe[15];
        int mode; // mode 1: weapons, mode 2: character
        if((i-x-9)%2==0) mode = 1;
        else mode = 2;
        if(mode == 1) strcpy(tipe, "weapons_");
        else strcpy(tipe, "characters_");
        strcat(outputFile, tipe);
        FILE *fp;
        char buffer[5000];
        struct json_object *parsed_json;
        struct json_object *nameJson;
        struct json_object *rarityJson;
        char jsonPath[300]="/home/raul/";
        int gachaVal;
        if(mode == 1) strcat(jsonPath, "weapons"), gachaVal = rand()%130;
        else strcat(jsonPath, "characters"), gachaVal = rand()%48;
        char jsonFileName[100];
        strcpy(jsonFileName, gachaFileName(jsonPath,gachaVal, 0));
        strcat(jsonPath, "/");
        strcat(jsonPath, jsonFileName);
        fp = fopen(jsonPath,"r");
        fread(buffer, 5000, 1, fp);
        fclose(fp);
        parsed_json = json_tokener_parse(buffer);
        json_object_object_get_ex(parsed_json, "name", &nameJson);
        json_object_object_get_ex(parsed_json, "rarity", &rarityJson);
        char rarity[2];
        strcpy(rarity, json_object_get_string(rarityJson));
        printf("rarity: %s\n", rarity);
        char name[100];
        strcpy(name, json_object_get_string(nameJson));
        printf("name: %s\n", name);
        strcat(outputFile, rarity); strcat(outputFile, "_");
        strcat(outputFile, name); strcat(outputFile, "_");
        char primoLeft[100];
        sprintf(primoLeft, "%d", primogems);
        strcat(outputFile, primoLeft);
        printf("outputFile: %s\n", outputFile);
        char txtPath[100];
        strcpy(txtPath, currFolder);
        strcat(txtPath, "/");
        strcat(txtPath, filename);
        printf("txtpath: %s\n", txtPath);
        snprintf(buf1, sizeof buf1, "%s", txtPath);
        FILE *data;
        data = fopen(buf1, "a+");
        fprintf(data, "%s\n", outputFile);
        fclose(data);
        i++;
        sleep(0.1);
    }
    else done = 1;
}
```
Dalam program diatas, gacha dilakukan dengan merandom angka sesuai jumlah file, contoh untuk weapons dilakukan random sebanyak 130, untuk character dilakukan random sebanyak 48. Kemudian hasil random tadi (gachaVal) dicocokkan dengan file json dengan index yang sama dengan gachaVal.
```c
char* gachaFileName(char *base, int gachaNum, int i)
{
    char path[1000];
    struct dirent *dt;
    DIR *dir = opendir(base);
    char *result;
    int found = 0;
    while ((dt = readdir(dir)) != NULL && found == 0)
    {
        if (strcmp(dt->d_name, ".") != 0 && strcmp(dt->d_name, "..") != 0)
        {
            result = dt->d_name;
            if (i == gachaNum) found = 1;
            i++;
        }
    }
    closedir(dir);
    return result;
}
```
Jika primogemsnya habis atau kurang dari 160, maka gacha selesai dilakukan
```c
if(primogems < 160) printf("Gachanya kelar bang, primogems-nya udah abis.");
```
Kemudian 3 jam setelah jam 4.44 yaitu jam 7.44, program akan mengzip folder gacha_gacha menjadi not_safe_for_wibu.zip dengan password "satuduatiga"
```c
else if(timeinfo.tm_mday == 30 && timeinfo.tm_mon + 1 == 3 && timeinfo.tm_hour == 7 && timeinfo.tm_min == 44)
        //else if(timeinfo.tm_mday == 19 && timeinfo.tm_mon + 1 == 3 && timeinfo.tm_hour == 19 && timeinfo.tm_min == 15)
        {
            // Jam 7:44 abis anniv
            if(!encrypted)
            {
                encrypted = 1;
                while(wait(NULL) > 0);
                pid_t pid = fork();
                if (pid < 0) exit(EXIT_FAILURE);
                if (pid == 0)
                {
                    char *arg10[] = {"zip", "-r", "not_safe_for_wibu.zip", "gacha_gacha", "--password", "satuduatiga", NULL};
                    execv("/bin/zip", arg10);
                }
```
kemudian program akan menghapus folder gacha_gacha, characters, dan weapons seperti berikut
```c
while(wait(NULL) > 0);
pid = fork();
if (pid < 0) exit(EXIT_FAILURE);
if (pid == 0)
{
    char *arg11[] = {"rm", "-r", "gacha_gacha", NULL};
    execv("/bin/rm", arg11);
}

while(wait(NULL) > 0);
pid = fork();
if (pid < 0) exit(EXIT_FAILURE);
if (pid == 0)
{
    char *arg11[] = {"rm", "-r", "characters", NULL};
    execv("/bin/rm", arg11);
}

while(wait(NULL) > 0);
pid = fork();
if (pid < 0) exit(EXIT_FAILURE);
if (pid == 0)
{
    char *arg11[] = {"rm", "-r", "weapons", NULL};
    execv("/bin/rm", arg11);
}
```
## Hasil program soal1
Berikut adalah isi didalam folder gacha_gacha
![image-1.png](https://drive.google.com/uc?export=view&id=18KbuiCHRPyRIgfUjTJiQJp6KCKIespN8)

Didalam file terdapat 10 hasil gacha seperti berikut
![image-2.png](https://drive.google.com/uc?export=view&id=1tthhq6a5GL7uaBvk3i9FKkr6qNgIhghJ)

Setelah 3 jam, folder gacha_gacha berubah menjadi not_safe_for_wibu dengan password "satuduatiga"
![image-3.png](https://drive.google.com/uc?export=view&id=13AwhLNJlqe26B4CZpNiZQu9INzOnrzBU)

# Soal 2
Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan  pekerjaannya.
1. Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder `“/home/[user]/shift2/drakor”`. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.
2. Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip. Contoh: Jenis drama korea romance akan disimpan dalam “/drakor/romance”, jenis drama korea action akan disimpan dalam `“/drakor/action”`, dan seterusnya.
3. Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama. Contoh: `“/drakor/romance/start-up.png”`.
4. Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus dipindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama `“start-up;2020;romance_the-k2;2016;action.png”` dipindah ke folder `“/drakor/romance/start-up.png”` dan `“/drakor/action/the-k2.png”`.
5. Di setiap folder kategori drama korea buatlah sebuah file `"data.txt"` yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending).

## Penjelasan Code Soal 2
Di fungsi main, pertama-tama dilakukan spawning process. Pada child process, kami menggunakan perintah unzip untuk melakukan unzip pada file-file yang berada di drakor.zip dan hanya mengekstrak file yang berekstensi ".png" pada direktori `"/home/raul/shift2/drakor"` dengan tag `"-d"`
Lalu, pada parent process, kami memanggil fungsi `master` yang menjadi fungsi file master pengelola utama, seperti membuat folder, memindahkan file, serta untuk menyetor data.
```c
int main()
{
    // main function
    char path[30] = "/home/raul/shift2/drakor";
    id_t child_id;
    int status=0;
    child_id = fork();
    if (child_id < 0) exit(EXIT_FAILURE);
    if (child_id == 0)
    {
        initFolder();
        char *argv[] = {"unzip", "drakor.zip", "*.png", "-d", path, NULL};
        execv("/bin/unzip", argv);
    }
    else
    {
        while(wait(&status) > 0);
        master(path);
    }
    return 0;
}
```
Sebelum program melakukan proses unzip, program akan membuat folder terlebih dahulu di path `"/home/raul/shift2/drakor"` dengan memanggil fungsi `initFolder` yang berisi:
```c
void initFolder()
{
    // membuat folder main-task di /home/raul/shift2/drakor
    int status = 0;
    if(fork()==0)
    {
        char buf1[256];
        snprintf(buf1, sizeof buf1, "shift2/drakor/");
        char *argv[] = {"mkdir", "-p", buf1, NULL};
        execv("/bin/mkdir", argv);
    }
    while(wait(&status)>0);
}
```
Lalu, dalam fungsi `master`, digunakan metode traverse secara rekursif yang serupa dengan yang ada pada modul. Namun, pada fungsi master ini terdapat beberapa langkah yang berbeda, yaitu penggunaan delimiter ";_" untuk mendapatkan format penamaan yang sesuai dan untuk mengklasifikasikan data menjadi lebih rapi. Selain itu, pada fungsi master ini, ketika terdapat dua informasi film drakor dalam satu penamaan file gambar drakor, maka kami membuat array data dengan dimensi 2 untuk menyetor informasi seputar drakor (nama pada index ke 0, tahun rilis pada index 1, dan genre/kategori pada index 2 untuk setiap informasi drakor yang ada pada nama file gambar yang diberikan. Setelah semua informasi dari nama file dalam drakor.zip berekstensi .png sudah ditampung dalam array data, setelah itu lakukan perulangan sebanyak jumlah drakor yang ada pada gambar. Kemudian, untuk setiap perulangannya, program akan memanggil fungsi `categoryFolder` yang akan menerima parameter kategori dari gambar yang diproses. Lalu, `moveFiles` dengan memanggil fungsi moveFiles yang menerima parameter kategori drakor, nama drakor, dan juga nama file gambar yang diproses. Setelah itu, program akan memanggil fungsi `storeData` yang akan menerima parameter kategori drakor, nama drakor, dan juga tahun rilisnya.
```c
void master(char *base)
{
    // file master pengelola file utama -> membuat folder, memindahkan file, dan menyetor data
    char path[1000];
    struct dirent *dt;
    DIR *dir = opendir(base);
    char *token;
    if (!dir) return;
    while ((dt = readdir(dir)) != NULL)
    {
        if (strcmp(dt->d_name, ".") != 0 && strcmp(dt->d_name, "..") != 0)
        {
            char namafile[700], *namefile;
            namefile = dt->d_name;
            snprintf(namafile, sizeof namafile, "%s", dt->d_name);
            token  = strtok(namafile, ";_");
            char *data[2][3];
            int n=0;
            while(token != NULL)
            {
                int i = 0;
                while(i<3)
                    data[n][i++] = token,
                    token = strtok(NULL, ";_");
                n++;
            }
            for(int i=0; i<n; i++)
                categoryFolder(data[i][2]),
                moveFiles(data[i][0], data[i][2], namefile),
                storeData(data[i][0], data[i][1], data[i][2]);
            delFiles(namefile);
            strcpy(path, base); strcat(path, "/"); strcat(path, dt->d_name);
            master(path);
        }
    }
    readData();
    closedir(dir);
}
```
Pada fungsi `categoryFolder`, program memiliki sebuah child process yang didalamnya akan membuat sebuah folder baru berdasarkan parameter yang diterima di dalam folder drakor dengan command `mkdir`. Tidak lupa juga, program menggunakan `-p` yang digunakan untuk menghiraukan error jika file yang akan dibentuk sudah ada sebelumnya.
```c
void categoryFolder(char *category)
{
    // membuat folder baru sesuai dengan kategori/genre-nya
    int status = 0;
    if(fork()==0)
    {
        char buf1[256];
        char *kategori = strtok(category, ".");
        snprintf(buf1, sizeof buf1, "shift2/drakor/%s", kategori);
        char *argv[] = {"mkdir", "-p", buf1, NULL};
        execv("/bin/mkdir", argv);
    }
    while(wait(&status)>0);
}
```
Sedangkan, pada fungsi `moveFiles`, program akan memindahkan file-file yang ada di `"/home/raul/shift2/drakor"` yang sama dengan variabel filename lalu dicopy ke folder `"/home/raul/shift2/drakor/kategori/judul-drakor.png"`. Penggunaan `snprintf` digunakan agar format file source (asal) dan file destinasi jelas. var buf1 akan menyimpan letak file asal, dan var buf2 akan menyimpan letak file tujuan. Lalu, kami membuat process baru pada child yang akan menjalankan proses move ini dengan menggunakan perintah `cp`.
```c
void moveFiles(char *filename, char *category, char *namafile)
{
    // memindahkan file hasil ekstrak ke dalam folder kategori sesuai genre
    int status = 0;
    char buf1[256], buf2[256];
    char *kategori = strtok(category, ".");
    snprintf(buf1, sizeof buf1, "shift2/drakor/%s", namafile);
    snprintf(buf2, sizeof buf2, "shift2/drakor/%s/%s.png", kategori, filename);
    if(fork()==0)
    {
        char *argv[] = {"cp", "-r", buf1, buf2, NULL};
        execv("/bin/cp", argv);
    }
    while(wait(&status)>0);
}
```
Pada fungsi `storeData`, kategori yang diparsing pada parameter masih terdapat ".png", maka solusinya adalah dengan menggunakan strtok untuk menghapus ".png"-nya. Setelah dihapus program menggunakan fungsi `snprintf` yang akan membuat string baru pada variabel buf1 yang akan berisi di mana letak data sementara disetor (data_prev.txt). Untuk membuat sebuah file keterangan yang baru digunakan fungsi `fopen`. Untuk menuliskan sesuatu untuk menyetor data pada file tersebut digunakan fungsi `fprintf`. Kemudian, program akan melakukan proses penyetoran data ke dalam file data_prev.txt sesuai dengan permintaan soal, yaitu kategori, nama, dan juga tahun rilisnya. Namun, karena kategori hanya muncul sekali sebagai header dari file data_prev.txt, maka akan dibuat suatu pengecekan apakah file .txt tersebut sudah ada atau belum. Jika file .txt tersebut belum ada, maka program akan menginput data kategori, nama, dan juga tahun rilisnya, sehingga proses penyetoran kategori hanya dilakukan sekali, yaitu pada saat pembuatan file datanya. Sebaliknya ketika file .txt sudah pernah dibuat, maka yang disetorkan ke dalam file .txt-nya hanyalah nama dan juga tahun rilis. Lalu, `fclose` digunakan untuk menutup file .txt tersebut.
```c
void storeData(char *judulDrakor, char *tahun, char *kategori_input)
{
    // setor data ke file .txt (data_prev.txt -> file .txt sementara)
    char *kat = strtok(kategori_input, "."), kategori[100], buf1[10000];
    char path[100] = "shift2/drakor/", filename[20] = "/data_prev.txt";
    int status = 0;
    strcpy(kategori, kat);
    snprintf(buf1, sizeof buf1, "shift2/drakor/%s/data_prev.txt", kategori);
    strcat(path, kategori);
    strcat(path, filename);
    FILE *data;
    if (data = fopen(path, "r"))
    {
        data = fopen(buf1, "a+");
        fprintf(data, "nama : %s", judulDrakor);
        fprintf(data, "\n");
        fprintf(data, "rilis : tahun %s", tahun);
        fprintf(data, "\n\n");
        fclose(data);
    }
    else
    {
        strcpy(category[catN++], kategori);
        data = fopen(buf1, "a+");
        fprintf(data, "kategori : %s", kategori);
        fprintf(data, "\n\n");
        fprintf(data, "nama : %s", judulDrakor);
        fprintf(data, "\n");
        fprintf(data, "rilis : tahun %s", tahun);
        fprintf(data, "\n\n");
        fclose(data);
    }
}
```
Setelah dilakukan pemanggilan terhadap ketiga fungsi tersebut di dalam loop, program akan memulai untuk menghapus semua file .png yang sudah diekstrak pada direktori `"/home/raul/shift2/drakor"` yang berisi file gambar .png dari drakor-drakor yang ada dengan fungsi delFiles. Metode yang digunakan pada fungsi ini adalah dengan memindahkan directory kerja ke `"/home/raul/shift2/drakor"` dengan menggunakan perintah chdir. Setelah itu, tinggal dijalankan perintah `rm` untuk menghapus file gambar drakor hasil ekstrak dari drakor.zip. Nama file yang dihapus didapatkan dari variabel namafile yang dikirim parameternya melalui fungsi master.
```c
void delFiles(char *namafile)
{
    if(fork()==0)
    {
        chdir("/home/raul/shift2/drakor");
        char *argv[] = {"rm", namafile, NULL};
        execv("/bin/rm", argv);
    }
}
```
Lalu, setelah segala proses pada fungsi master berjalan, maka fungsi `readData` akan dipanggil sebelum menutup direktorinya. Pada fungsi readData, program akan membaca kembali data-data yang terisi pada file data_prev.txt untuk kemudian diklasifikasikan yang mana yang termasuk judul dari drakor, yang mana merupakan tahun rilis drakor, serta yang mana merupakan kategori dari drakor tersebut. Untuk judul drakor, pola yang ada pada file adalah berada pada kelipatan dari 3, sehingga ketika counter fungsi yang menotasikan line yang sedang dikunjungi memiliki kelipatan 3 (counter%3 bernilai 0), maka akan dilakukan pengklasifikasian string untuk mendapatkan nilai dari judul drakornya. Begitu pula dengan metode pembacaan tahun rilis drakor yang memiliki pola bersisa 1 ketika dimoduluskan dengan 3 (berkelipatan 4).
Kemudian, fungsi readData akan memanggil fungsi `sortByTahun` untuk mengurutkan data drakor berdasarkan tahun rilis yang paling lama. Lalu, terdapat fungsi printData yang kami gunakan untuk mendebug hasil dari klasifikasi fungsi readData. Selanjutnya, terdapat fungsi `deletePrevData` yang akan menghapus seluruh file `"data_prev.txt"` yang ada di setiap folder kategori pada path `"/home/raul/shift2/drakor"`. Terakhir, terdapat pemanggilan fungsi `writeData` untuk menuliskan kembali data-data yang sudah berhasil disortir atau diurutkan berdasarkan tahun rilis ke dalam file `"data.txt"` yang ada di folder masing-masing kategori film drakor.
```c
void readData()
{
    // baca data dari text file di masing-masing kategori, setor data ke struct, dan panggil sort untuk mengurutkan
    for(int i=0; i<catN; i++)
    {
        char path[100] = "shift2/drakor/";
        strcat(path, category[i]);
        char filename[20] = "/data_prev.txt";
        strcat(path, filename);
        FILE *fp = fopen(path, "r");
        const unsigned MAX_LENGTH = 256;
        char buffer[MAX_LENGTH];

        int counter = 1, drakorCounter = 0;
        film drakor[20];
        while(fgets(buffer, MAX_LENGTH, fp))
        {
            if(counter%3==0)
            {
                char judul[50];
                char *token = strtok(buffer, " ");
                while(token!=NULL) strcpy(judul, token), token = strtok(NULL, " ");
                if(strcmp(judul," ") != 0)
                    strcpy(drakor[drakorCounter].judul, judul);
            }
            else if(counter%3==1 && counter>3)
            {
                char read[50];
                char *token = strtok(buffer, " ");
                while(token!=NULL) strcpy(read, token), token = strtok(NULL, " ");
                if(atoi(read) > 0)
                    drakor[drakorCounter++].tahun = atoi(read);
            }
            counter++;
        }
        // panggil sort
        sortByTahun(drakor, drakorCounter);
        printf("KATEGORI %s:\n", category[i]);
        printData(drakor, drakorCounter);
        deletePrevData(category[i]);
        writeData(drakor, drakorCounter, category[i]);
        fclose(fp);
    }
}
```
Program memiliki `struct film` yang mengandung judul dan juga tahun rilis dari suatu film. Hal ini dilakukan untuk mempermudah proses penyortiran pada setiap kategori film drakor yang ada. Kemudian, terdapat variabel `catN` yang digunakan untuk mencatat berapa banyak kategori yang ada pada drakor.zip. Sedangkan, variabel `category` digunakan untuk mencatat kategori apa saja yang ada pada drakor tersebut.
```c
typedef struct film
{
    char judul[50];
    int tahun;
} film;

int catN = 0;
char category[20][20];
```
Pada fungsi `sortByTahun` memiliki parameter film yang merupakan struct yang dibuat sebelumnya dan juga n yang merupakan jumlah drakor yang termasuk ke dalam suatu kategori tertentu. Fungsi sortByTahun ini digunakan untuk menyortir drakor di masing-masing kategori menjadi terurut berdasarkan tahun rilis (dari yang terlawas menuju ke yang terbaru). Metode penyortiran yang program gunakan adalah selection sort, sebuah teknik pengurutan dengan cara mencari nilai tertinggi ataupun terendah di dalam array (dalam konteks ini menggunakan struct film) kemudian menempatkan nilai tersebut di tempat semestinya.
```c
void sortByTahun(film curr[], int n)
{
    // sort data drakor berdasarkan tahun
    film drakortemp;
    for(int i=0;i<n;i++) for(int j=i+1;j<n;j++)
    if(curr[i].tahun > curr[j].tahun)
        drakortemp = curr[i], curr[i] = curr[j], curr[j] = drakortemp;
}
```
Fungsi `printData` digunakan untuk melakukan debugging terhadap hasil penyortiran maupun hasil migrasi `data_prev.txt` menuju ke `data.txt`:
```c
void printData(film curr[], int n)
{
    // ngeprint semua data yang ada pada struct untuk testing
    for(int i=0;i<n;i++) if(curr[i].tahun > 0) printf("Judul: %s\nTahun: %d\n\n", curr[i].judul, curr[i].tahun);
}
```
Fungsi `deletePrevData` digunakan untuk menghapus file `"data_prev.txt"` (file penyimpanan data sementara yang belum disortir berdasarkan tahun rilis) yang ada pada setiap kategori film drakor dengan menggunakan metode yang sama dengan fungsi `delFiles`.
```c
void deletePrevData(char *kategori)
{
    // hapus file data_prev di setiap kategori
    if(fork()==0)
    {
        char path[100] = "shift2/drakor/";
        strcat(path, kategori);
        chdir(path);
        char *argv[] = {"rm", "data_prev.txt", NULL};
        execv("/bin/rm", argv);
        printf("deleting..\n");
    }
}
```
Lalu, dengan metode yang serupa yang digunakan fungsi `storeData`, program akan menulis kembali data-data hasil sortiran fungsi `sortByTahun` ke dalam file `"data.txt"` yang diberikan sebagai ketentuan pada soal sehingga problem nomor 2 (a-e) berhasil disolve.
```c
void writeData(film sorted[], int n, char kategori[])
{
    // tulis balik filenya
    for(int i=0; i<n; i++)
    {
        char buf1[10000];
        snprintf(buf1, sizeof buf1, "shift2/drakor/%s/data.txt", kategori);
        char path[100] = "shift2/drakor/";
        strcat(path, kategori);
        char filename[10] = "/data.txt";
        strcat(path, filename);
        FILE *data;
        printf("%s\n", buf1);
        if(sorted[i].tahun > 0 && sorted[i].tahun <= 2022)
        {
            if(data = fopen(path, "r"))
            {
                data = fopen(buf1, "a+");
                fprintf(data, "\nnama : %s", sorted[i].judul);
                fprintf(data, "rilis : tahun %d", sorted[i].tahun);
                fprintf(data, "\n");
                fclose(data);
            }
            else
            {
                data = fopen(buf1, "a+");
                fprintf(data, "kategori : %s", kategori);
                fprintf(data, "\n\n");
                fprintf(data, "nama : %s", sorted[i].judul);
                fprintf(data, "rilis : tahun %d", sorted[i].tahun);
                fprintf(data, "\n");
                fclose(data);
            }
        }
    }
}
```
## Kendala pada Soal Nomor 2
Debugging yang lumayan menghabiskan banyak waktu untuk melakukan penyortiran data, serta problem segmentation error yang kami dapatkan di beberapa case tertentu.
## Hasil Program soal2.c
![image-1.png](https://drive.google.com/uc?export=view&id=1-MmiIgXMV-_Mt_DyUoVxD3RaiSPeVD_P)
![image-2.png](https://drive.google.com/uc?export=view&id=1swCwqMRap4vvbU35Y7ZMRY62die6VGAs)
![image-3.png](https://drive.google.com/uc?export=view&id=1tfXmNRgaBellFNFv0h_w33K4vu4lCc6f)
![image-4.png](https://drive.google.com/uc?export=view&id=1b_K2Yz1XxexbnmdztlRZ5MDL91lx66cm)

# Soal 3
Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang
- Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”. 

- Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.
Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

- Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.

- Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png

## Penjelasan kode
Pada soal ini, kita diminta untuk membuat direktori, namun tidak boleh menggunakan mkdir(). Kita dapat menggunakan execv sebagai pengganti, namun sebelum menggunakan execv, kita terlebih dahulu melakukan fork() untuk menjalankan proses baru. Untuk melaksanakan fork(), kita menggunakan nested if, dan setiap else dari if, kita menunggu status fork() dari if sebelumnya (child-nya). Setelah itu, maka dapat melakukan fork() kembali.
```
void master(char path[]){
    pid_t child_id;
    int status = 0;
    child_id = fork();
    if(child_id < 0)
        exit(EXIT_FAILURE);
    else if (child_id == 0){
        printf("Membuat folder darat...\n");
        initFolderDarat();
        
    }else {
        pid_t child_id2;
        int status2 = 0;
        child_id2 = fork();
        while(wait(&status)>0);
        sleep(3);
        if (child_id2 == 0){
            printf("Membuat folder air...\n");
            initFolderAir();
        }else{
            pid_t child_id3;
            int status3 = 0;
            child_id3 = fork();
            if(child_id3 == 0){
                printf("Unzipping animal.zip ...\n");
                unzipFile();
            }else{
               int status4 = 0;
               while(wait(&status3) > 0);
               if(fork() == 0){
                   char pathAir[] = "/home/afdal/modul2/air";
                   char air[] = "*air*";
                   char pathDarat[] = "/home/afdal/modul2/darat";
                   char darat[] = "*darat*";

                   printf("Moving air animal to air folder\n");
                   moveFile(pathAir, air);

                   printf("Moving darat animal to darat folder\n");
                   moveFile(pathDarat, darat);

                   printf("Deleting the rest\n");
                   deleteFile();
               }else{
                   int status5 = 0;
                   while(wait(&status4) > 0);
                   if(fork() == 0){
                       printf("Deleting birds\n");
                       deleteBird();
                   }else{
                       while(wait(&status5) > 0);
                       listFile();
                   }
               }
            }
        }
    }
}
```
Dapat dilihat bahwa terdapat sleep(3). Fungsi sleep() adalah fungsi untuk memberhentikan program sementara selama parameter yang diberikan. Karena pada persoalan diminta untuk menunggu selama 3 detik, maka kita gunakan sleep(3). Dari potongan kode diatas, kita dapat melihat pola if else sebagai berikut
```
if(fork() == 0)
    execute program
else
    wait status
    if(fork() == 0)
        execute program
    else
        wait status
        if(fork() == 0)
            execute program
        else
            ...
            ...
```
Pola tersebut adalah hubungan dari menjalankan proses child dan parent. Ketika proses child selesai, maka akan dilanjutkan proses parent nya. Kita melakukan proses yang diminta pada persoalan di bagian execute program, sesuai dengan yang diminta terlebih dahulu.

### Membuat folder darat dan air
```
void initFolderDarat()
{
    // membuat folder main-task di /home/afdal/shift2/drakor
   	 int status = 0;
   	 if(fork()==0)
   	 {
   		 char *argv[] = {"mkdir", "-p", "/home/afdal/modul2/darat", NULL};
   		 execv("/bin/mkdir", argv);
   	 }
   	 while(wait(&status)>0);
}
```
Karena tidak dapat menggunakan mkdir() dan system(), kita dapat menggunakan execv. Sebelum menjalankan execv, kita melakukan fork() untuk membuat proses baru. Di dalamnya, kita jalankan execv. Karena yang diminta adalah membuat folder darat dan air, maka parameter pada execv yaitu /bin/mkdir. Parameternya yaitu sesuai dengan proses mkdir pada bash, yaitu option dan path direktori yang ingin dibuat. Perbedaan fungsi untuk membuat folder air dan darat hanya pada nama pathnya saja. Ketika proses fork() selesai, maka kita menunggu statusnya hingga selesai sehingga kita dapat melakukan proses berikutnya.

### Unzip animal.zip
```
int status = 0;
   	 if(fork()==0)
   	 {
   		char *argv[] = {"unzip", "-q", "-o", "/home/afdal/animal.zip", "-d", "/home/afdal/modul2", NULL};
        execv("/bin/unzip", argv);
   	 }
   	 while(wait(&status)>0); 
```
Mirip dengan membuat folder, perbeadan proses zip hanya terletak pada parameter dari execv nya. Karena kita ingin melakukan unzip, maka kita jalankan /bin/unzip dengan parameter proses unzip seperti biasa yaitu option, path, dan destination.

### Memindahkan file sesuai dengan jenis hewan
```
void moveFile(char path[], char file[]){
     int status = 0;
   	 if(fork()==0)
   	 {
   		char *argv[] = {"find", "/home/afdal/modul2/animal", "-name", file, "-exec", "mv", "-t",path, "{}","+",NULL };
        execv("/bin/find", argv);
   	 }
   	 while(wait(&status)>0); 
}
```
Untuk memindahkan file, kita dapat menggunakan proses find terlebih dahulu. Proses find akan mencari file dengan kriteria yang diberikan, dan melakukan sebuah proses terhadap file yang ditemukan. Karena kita ingin memindahkan file, maka setelah kita find, kita menggunakan -mv. Maka kita jalankan execv /bin/find dengan parameter find pada umumnya, yaitu direktori, kriteria, dan proses selanjutnya. Pada fungsi ini, path[] berarti destinasi, dan file[] merupakan kriteria yang diinginkan.

### Menghapus file yang tidak memiliki jenis
```
void deleteFile(){
    int status = 0;
   	 if(fork()==0)
   	 {
   		char *argv[] = {"find", "/home/afdal/modul2/animal", "-name", "*.jpg*", "-exec", "rm", "{}","+",NULL };
        execv("/bin/find", argv);
   	 }
   	 while(wait(&status)>0); 
}
```
Nama file yang tidak memiliki jenis apapun memilki sebuah kesamaan, yaitu semuanya pasti memiliki .jpg pada nama filenya. Sehingga kita lakukan find pada directory animal, dan proses selanjutnya yaitu remove (rm).

### Menghapus hewan dengan jenis bird di folder darat
```
void deleteBird(){
   int status = 0;
   	 if(fork()==0)
   	 {
   		char *argv[] = {"find", "/home/afdal/modul2/darat", "-name", "*bird*", "-exec", "rm", "{}","+",NULL };
        execv("/bin/find", argv);
   	 }
   	 while(wait(&status)>0);  
}
```
Sama seperti menghapus pada proses menghapus sebelumnya, kali ini kita hanya mengubah direktori yang di baca, dan kriteria yang diinginkan.

### Membuat list.txt pada folder air yang berisi list nama file dengan format UID_[UID Permission]_[Nama File].jpg
```
FILE *fptr;
    struct dirent *dt;
    char path[] = "/home/afdal/modul2/air/";
    DIR *dir = opendir(path);

    fptr = fopen("/home/afdal/modul2/air/list.txt", "w");
    if(fptr == NULL){
      printf("ERROR!\n");
      exit(1);
    }
```
Potongan kode di atas adalah untuk membuka direktori air (opendir(path)) dan membuka file list.txt. 

```
while ((dt = readdir(dir)) != NULL){
      struct stat fs;
      int r = stat(path, &fs);
      if(r == -1){
        fprintf(stderr, "File Error\n");
        exit(1);
      }
      register struct passwd *pw;
      uid_t uid = getuid();
      pw = getpwuid(uid);
      if(strcmp(dt->d_name,".") != 0 && strcmp(dt->d_name, "..") != 0 && strcmp(dt->d_name, "list.txt") != 0){
        char userp[256] = "";
        if(fs.st_mode & S_IRUSR)
          strcat(userp, "r");
        if(fs.st_mode & S_IWUSR)
          strcat(userp, "w");
        if(fs.st_mode & S_IXUSR)
          strcat(userp, "x");
        fprintf(fptr,"%s_%s_%s\n",pw->pw_name,userp, dt->d_name);
      }
    }
    fclose(fptr);
```
Segala data direktori yang dibaca akan disimpan pada struct dt. Selama direktori masih dapat dibaca, maka kita melakukan proses. Struct stat fs berfungsi sebagai struct yang menyimpan status dari file yang sedang dibaca. Untuk mendapatkan username, kita dapat menggunakan proses berikut
```
    register struct passwd *pw;
    uid_t uid = getuid();
    pw = getpwuid(uid);
```
Username yang mengakses file disimpan pada pw->pw_name . 

Karena direktori "." dan ".." tidak dimasukkan ke dalam list, maka kita exclude. Begitu juga dengan list.txt . dt->d_name menyimpan nama file yang sedang dibaca. Untuk mengecek permission dari file yang dibaca, kita dapat menggunakan potongan kode berikut
```
if(fs.st_mode & S_IRUSR)
    strcat(userp, "r");
if(fs.st_mode & S_IWUSR)
    strcat(userp, "w");
if(fs.st_mode & S_IXUSR)
    strcat(userp, "x");
```
Setelah itu, kita melakukan fprintf ke file list.txt sesuai format yang diminta.

## Kendala pada Soal Nomor 2
Sedikit sulit untuk bagian terakhir pas bikin list.txt . Karena harusnya "." ".." masuk ke iregular file, tapi ditempat saya lolos ke regular file. Jadinya saya liat nomor 2 yang udha duluan selesai, pake if. Kalau dilihat juga, banyak yang ngambil potongan kode dari soal no 2 yang dibikin raul, bahkan komennya belum saya ubah.
## Hasil Program soal2.c
![image-1.png](https://drive.google.com/uc?export=view&id=1ipq0owq7jDuC_5ZyaoCkwdKETzuNzQM4)
![image-2.png](https://drive.google.com/uc?export=view&id=1dq4UB9HBHg8BhRMeT_queqNBik1s6nwh)
![image-3.png](https://drive.google.com/uc?export=view&id=1lz2tBqOj6-XSiuMbwkK-iBzloGZROfJf)
![image-4.png](https://drive.google.com/uc?export=view&id=1R19sFYmiyxO7j6zJcwB8rCfKXREXFpQr)
